# Why record a video

* We can't have every group have their presentation, upload on YouTube can help us to have all group presentation.


# What should I record

* Record your screen with your voice include your face if possible.

* You don't need to edit your video.

* Make sure your video is clear and your voice is loud.

* Each video should be around 2-4 minutes.

# How to record

* You can use any software to capture your presentation and voice.

* Below shows that capture screen and voice using OBS and upload on YouTube.


# How to record screen using OBS

1. Download OBS from [OBS homepage](https://obsproject.com/welcome).

2. Go to Auto-Configuration Wizard. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_1.png)

3. Select optimize for recording. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_2.png)

4. Click next. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_3.png)

5. Click apply settings. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_4.png)

6. For changing recording format click setting. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_5.png)

7. Click output then change recording format to mp4. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_6.png)

8. Display capture. Click '+' → Display Capture. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_7.png)

9. Click next.  ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_8.png)

10. Click ok. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_9.png)

11. Setting complete. Click start recording to start.  ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_10.png)

12. OBS will capture your microphone automatically. If it didn't click '+' → Audio Input Capture. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_11.png)

13. Click OK. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_12.png)

14. Select your device. Click OK. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/OBS_13.png)



# How to upload video on Youtube

1. Go to your channel and click UPLOAD VIDEO. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/YT_1.png)

2. Click SELECT FILES. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/YT_2.png)

3. Use the following format for title <group>_<week> and write down your group member. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/YT_3.png)

4. Click next. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/YT_4.png)

5. Click next. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/YT_5.png)

6. Click next. ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/YT_6.png)

7. Select Public. If you prefer not to make your video public, select Unlisted ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/YT_7.png)

8. Copy your video link and post it in [Google sheet](https://docs.google.com/spreadsheets/d/1FJ47smHB1JHGm_l5yGB0FVr8EQMDhTEKzvfjo4xibE0/edit) ![](https://bitbucket.org/temn/nordlinglab-profskills/downloads/YT_8.png)
