







# 2021-10-10 #

* I finally created my git diary after many failed attempts (and writing on my own repository for 2 weeks)
* One of my friends got kidney stones because of drinking too much coke, I think I'll stop drinking it after that
* This class was interesting but I still got distracted all the time, my medicine is not working as good as it used to.
* I bought a Switch so I can relax and play between classes, the videos the professor showed us are really interesting to watch outside of class, but they're a bit distracting for me during class (I end up watching other videos)

# 2021-10-21 #	

* This class was quite interesting, it touched some topics that really interest me.
* My meds don't work at all and I still get distracted during calss.
* I forgot to talk to my teammates after I contacted them.
* The videos are interesting but sometimes the timing of the class gets messed up by the videos.

# 2021-10-28 #

* This class was really interesting as it touched few themes that I'm familiar with. 
* I was diagnosed with depression and ADHD a few years ago and this class gave me some insight on my own feelings of anxiety. 
* Around 80% of engineering students have depression or anxiety..
* The future of the earth and climate change dworsen my feelings and generate some anxiety. 

# 2021-11-14 #
* This class was about the big group presentations, these were the themes talked about:
 * 1- Increasing quality of sleep improves learning.
 * 2- Materials that can be used to make buildings sustainable.
 * 3 - Some materials that can be used to replace steel reinforced cement in buildings to reduce CO2 emissions.
 * 4 - Some projects to compensate for local CO2 emissions that lead to net zero emissions.
 * 5 - Successful strategies to promote climate action and sustainable development.
 * I had my Covid shot this week and it derailed all that I had planned completely, the fever and the pain were so big that I couldn't accomplish anything meaningful this week, the stress was immense.
 * I'm consuming a really big amount of stimulants to keep up with my exams and classes but I'm afraid that all this work won't be enough.
 
# 2021-11-28 # 
  
 * All the groups talked about contamination in Taiwan and in other countries
 * The situation in my country is really bad , the government and the law do not help at all, all the regulations were violated and some new regulations just gave more power to the Agronomical industry, destroying one of our biggest forests. 
 * The topic of this class was the problem with spending too much time on the internet.
  
# 2021-12-12 # 
  
 * I broke my arm yesterday in a rugby game, It hurt a lot but i had a lot of fun after they put my dislocated shoulder back in place
 *  The class was interesting, talking about our many doomsday clocks (all the possible ways we can destroy our planet)
 * The future is bleak, but my arm will be alright
 
# 2021-12-19 # 
  
 * I couldn't go to class this week because of my arm, I went to the hospital and did some exercises because of physical therapy.
 * My arm is recovering really fast, I can move it now but the range of motion is still not the same as my left, and it hurts when I do certain movements.
 * I feel like the tasks for the big group are a bit too hard to complete, since we're in the finals season and I have weekly exams on one of my classes and that takes too much time from me.
 * I still like the teaching methods in person because it makes the classes more interesting.
  
# 2021-12-23 # 
  
  * I couldn't go to class this week because of my arm, I had a small relapse and it was really painful for me to move, I could barely get home after my first class today.
  * I tried to join the class online but there was trouble when connecting to it, I restarted my computer when the teacher let us get in so I couldn't access the class after that.
  * I'll try to go physically next time because I don't want to miss on the content.
  
   * I'll try to go physically next time because I don't want to miss on the content.

  

   # 2021-12-30 # 

   * The teacher gave us another opportunity to finish the diary.

   * I was successful today because I managed to go to all my classes2

   * I was productive too as I managed to win one of the projects we had for my class and finished a really important ppt for it too.

   * I did the presentation for the big group too and I feel like things are better now, I've struggled a bit lately with my attention deficit, mainly because I'm not medicating anymore (I had panic attacks under the medication), but I've been slowly building up on it and I'm doing great.

   * If I keep working towards the future I might be good to go.

   

   # 2021-12-31 #

  * I was not productive today but I sure am successful, I managed to rest for today.

  * I found myself spending new year's eve alone, I've been doing that since I came to Taiwan, but my condition has changed greatly each year.

  * My first year here I spent the night watching movies and eating pizza, the transition felt nice but I missed a lot of people back in my country, I felt alone but hoping that things would improve once 2020 started (It didn't)

  * My second year here I spent the night in a completely different city by myself, got some cheap alcohol and dangled with certain bad ideas, that was the last time I seriously thought about suicide (and almost attempting it).

  * 2021 has been the best (and longest) year of my life until now, I'm making peace with the fact that both my mind and my life will probably never be the same, I'm spending new year's eve alone again, but this time by choice and I'm really happy of all the growth I have done.

  * For 2022 I only hope I can get wiser and learn a lot more, I've thought of many things as negative in my college life, but I'm falling in love with the process and engineering, I hope that love will help me push through all my difficulties.

  * For tomorrow, I only hope I can play a lot of Zelda and see my friends.

# 2022-01-01 #

 * I was not productive but I was successful, I didn't see my friends but I had a really peaceful day.
 * I didn't put any work towards my projects because most stores were closed so I couldn't buy the materials that I needed.
 * I started cooking again since now I'm on a budget.
 * I hope I can overcome my executive dysfunction and be able to cook and study everyday.
 * Tomorrow I'll try to see my friends and prepare for my classes.

# 2022-01-02 # 

* I was productive and successful, I managed to put on some work with a certain organization I'm part of.
* I still didn't start on my other projects today but I'll start tomorrow.
* I played some videogames and relaxed most of the day, the real work starts on monday.
* I will keep trying until I am finished with all my projects, failure is not really an option but if I fail it's still alright, I will try my best to overcome all of my problems, there's still time for me to be really really successful, I will try not to waste it.
  
  
# 2022-01-03 #  

* My day has just started and I feel productive already, I've cooked my lunch and I have started working on my projects and studying too.
* I will probably reupdate the whole diary after the end of the day to add more stuff to it.
* If I keep working like this I'm sure I will manage to finish all my projects and even have some spare time to study a bit more. 

  

# 2022-01-04 # 

  

  

# 2022-01-05 # 

  
