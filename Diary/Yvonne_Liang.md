This is a diary written by Yvonne Liang(梁瑄芳）E14081224

# 2021-09-30 

* I learnt how to use data in a proper way.
* Professor told about the importance of cite.

# 2021-10-07 

* We learn about how to check if it is a fake news.
* Someone asked in a team, we should listen to the leader or to be skeptical to everything, and the professor said we should trust ourselves.
* We had a quick view about financials.
* A poll result doesn't tell 100% of the truth.

# 2021-10-14 

* Today we learn about financial, the technical term for fainancial are still so difficult to understand.
* I learnt how to read a chart correctly, and also learnt how to determine the right chart for the aimed topic.
* I thought the inequality in Taiwan isn't a big issue, but after the presentation the other group made, I find it actually a problem now in Taiwan.

# 2021-10-21 

* I forgot to write down the diary for this week.
* This week we talk about fictions and learn about other country's stories.
* In one video presentation, I learnt that there's a system of sharing bottle in Taiwan now.
* Sharing economy is now more popular in Taiwan and is better for the environment, also it's a change in our lifestyle. 

# 2021-10-28 

* In todays lecture, we learn about health. Last week we talk about physical health and make presentation for this week then we learn about mental health for this week .
* We do a poll about eco-anxiety.
* I watched the ted-talk about how to connect with depressed people when we decided to. I get the answer just chat with them normally.
* In the other video, the speaker tell us when we have our loved one suicidal, what important to do is listen to understand.
* The Golden Gate Bridge in San Francisco with it gorgeous view turned out to be a bridge of suicide since its opening. 
* A suicidal person doesn't have hope in their Pandora's box. The hope can come from someone who hear them, no blame and just being there can be a turning point.

# 2021-11-04 

* Today we heard our classmats share how they overcome their depression, I think they are courageous inspirational.
* Many of us feel depressed often but most of the time we thought that we're just so called "Strawberry Generation". In fact we are depressed and need help immediately.
* We heard our classmate share about their experience on working. But to tell the truth, I fell asleep for a while and missed quite a few parts.
* We are reported the final result about the poll we did at week 7, which is about climate anxiety. Professor said the number of people that has climate anxiety is more than expected.

# 2021-11-11 
* supergroup presentation

# 2021-11-18 
* no class for today

# 2021-11-25 
* Some groups introduces the law for noise limit in Paraguay.
* We watch the TED talk of the Father of virtual reality. I didn't know him before this lecture. I only know Audrey Tang.
* It's pretty special to know that there's no law for green house gas restrictions in Japan.
* Foreigners in Japan can get the health ID card in 14 days, but in Taiwan, foreigners should stay for over 6 months.
* Health insurance in Japan insure they have 70% discount on regular meal expense in the hospital.
* We debate on the topic on how to set up unforceable laws to prevent people from brain washing but also have the right of free speech, one gruop says that when the only way is to believe the god in our mind then everything will be fine, I think it does make sense.

# 2021-12-02 
* Today's lecture we shuold hand out our phone before class and count how many times we are finding our phones. I think it can be more challenging if our laptop, ipad or wifi is also limited. Or we just count the times we open the page of line, messenger, instagram etc.
* If it was how many times I open the page of social media on my laptop, I did it many times in class and can't actually count how many. Wifi is the most dangerous power that makes us feel almighty.
* We try to figure out some feasible ideas that we can imply on organizations or company in Tainan. Professor says that it should be done and to make us feel powerful to make some changes for our world, actually I'm afraid for most of the time.

# 2021-12-09 
* At first I'm worrying about how will the paper money industry live their live if paper money was banned, but I feel better when I know that we're always creating traditions, now and then, and it just take time to change, I believe those traditional industries will find their way out.


# 2021-12-16 
* Surprising to know having one fewer child is the most impactful action to take to reduce emissions of carbon dioxide
* The result from physical vote compare to online vote make so much difference

# 2021-12-23 
* 211223 Thu. 
*  A. Unsuccessful and productive.  
*  B. I felt unsuccessful because my bike tire flat during my way home after class. I felt productive because I complete my fluid mechanics homework before the due date and got most of the points. 
*  C. I will try to finish the fluid mechanics homework then I can have more time study on it instead of concentrated study before the due date. 

* 211224 Fri. 
*  A. Successful and unproductive. 
*  B. I felt successful because the experiment of electronic went nothing wrong, my group finished it soon and I was able to go back Kaohsiung early. I felt unproductive because when I went home I didn't study at all, I felt pretty tired and fell asleep early. 
*  C. I think I can take some vitamin pill to keep myself more energetic if I feel pretty tired. 

* 211225 Sat. 
*  A. Successful and unproductive 
*  B. I felt successful because I have a great time chatting with my family and watched soap opera together. I felt unproductive because I also didn't study at all. 
*  C. If I write down my todo list for today, I may be more accurate to know what I should finish first and will not watch TV all day. 

* 211226 Sun. 
*  A. Successful and unproductive 
*  B. I felt successful because I catched the earlier train back to Tainan, which I didn't expect to make it on time. I felt unproductive because I walked about 3 km back home because my bike tire problem wasn't fixed, then I watched drama instead of studying.
*  C. Self- discipline has long been my problem. I will try to tell myself study first then entertain.

* 211227 Mon.
*  A. Successful and unproductive
*  B. I felt successful because my bike's flat tire was fixed. I felt unproductive because I spent more time and expense than I expected, my tire problem wasn't simple.
*  C. Maybe next time I should carry my bebit card then I won't walk back home again to pick up money from the post office.

* 211228 Tue.
*  A. Successful and productive. 
*  B. I felt successful because I'd done my best on the Mechanism's quiz. I felt productive because I'm going to go to sleep at a normal time today.
*  C. I should try to go to sleep at normal time then I can feel energetic the next day.

* 211229 Wed.
*  A. Successful and productive.
*  B. I felt successful because I rode bycicle to Anping with my classmates in PE class. I felt productive because I felt so good after riding bike to Anping thus have done one of the homework after getting home instead of fallinf asleep.
*  C. Excercise truely can make me feel happy thus making me productive. I should excercise more but not so much that makes me feel tired.