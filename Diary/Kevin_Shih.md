This diary file is written by Kevin Shih H34075025 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* Conflicts are divided in 9 stages, separated in 3 levels
* A mediator is very important to solve a conflict
* Format and source on power points are a important professional skill, copyright on usage of image can be a bigger pain than you can imagine
* Genoma sequence is also exponentially growing
* Importance of batteries for the future and eletric vehicles
* Overall lecture had new facts for me and was quite interesting
# 2021-09-30 #
* fake news/disinformation are becoming a serious problem
* who, how, when, where, you always must ask yourself this questions when trusting some news
* you must be careful with statistics, there are many variables that could impact the final result
# 2021-10-07 #
* Money constraint our abilty to live
* Highest spike on loans in NTD is because of red envelopes on the end of year
* Money is create through loans
* Dolar power decrease a lot over the years
* Overall today's lecture had lots and lots of information about finance. I'm not really knowledgeble on this area
# 2021-10-14 #
* today we saw some TED talks about facism, I only had a rough idea on how it was nowadays and these videos gave me a insight
* many of the members of a facism groups are marginalized people that wanted to belong to a group, and not because of the cause
* with potholes even a good person can do wrong decisions when manipulated by others
* it's interesting to see how someone can become so ignorant about others until they see other people's perspective
# 2021-10-21 #
* today's class was really relatable since I have many problems with anxiety
* Alan watkins gave a perfect explaination on how our body is direct related to our behaviour
* It was already in my knowledge that exercise have many benefits to our mental health
* the last ted talk that we watched was quite boring
# 2021-10-25 #
* today we had our first super group presentation, I panicked a bit on how many members we had and how we would organize everything but in the end we did alright
* depression is a very heavy topic to discuss, we watch many stories about people that had experienced it in different perspectives
* sometimes just listening to what a people want to say might mean much more than you can imagine
* after listening all these stories, it left a bad taste in my mouth, but I also agree it's a topic that must be discussed
* our new projects are hard
# 2021-11-04 #
* some people shared their experiences with depression and anxiety, to be honest it was a pretty relatable moment
* anxiety has a really heavy impact on college students, I got curious on how english speakers would have to do if they wanted pychologist support
* find purpose on life was already something I was aware, and I  still chasing for one until this day, but it really is not a easy task
# 2021-11-11 #
* today we have many big presentations of many topics
* My group presented about reducing co2 emissions,I was a bit worry in the beginning because I thought it was a hard topic but in the end everything went ok
* Some facts presented were already part of my knowledge but not in depth, so it was interesting to listening in more detail
* This last week was a hell of exams and projects
# 2021-11-25 #
* we had our first physical lecture today, it was pretty fun to met my collegues that I was only able to see via call until now
* the topic today was pretty interesting, discussing about privacy, use of data and how vulnerable we are in the internet
* we group up together to come with a solution and it was damn hard to come up with something decent to present
# 2021-12-03 #
* We had the supergroup presentations today
* I couldn't go to physical class today so I lost a part of the experience, professor propose a experiment related to our dependency of smartphones
* the groups discuss many ideas, I didn't had any expectations but some ideas were actually feasible and realistic
* regarding to noise problems from constructions in taiwan, it's indeed a problem and the authorities usually neglect these cases
* some groups also had the same ideas as my group like the use of EVs, it was interesting to see different perspectives on the same solution
# 2021-12-10 #
* we did your presentation about the differences of newspapers headline, I find very interesting how every media is have somehow politics involved
* classifying news articles' political position is hard with only one article and without knowing about the political parties on the respective country
* we learnt about planetary boundaries, it surprised me that the atmosphere is not the higher risk danger but rather the soil
# 2021-12-16 #
* today's lecture was quite different, teacher bring one representative in the front to discuss the topic
* this project was quite a mess but I guess we made it through it
* our presentation got finished in a very unexpected way
* after listing all the presentation I think the PDF and church noise ideas are the most feasible ones
# 2021-12-26 #
* today presentations were like a summary of the video about the third industrial revolution
* It cover many questions that I had in the beginning of the course
* we had a fun discussion when we got divided by capitalists, workers and environmentalists
* we got a quick summary about success, it's kinda funny because I've heard some of the topics covered on another GE course, learn to succeed
# 2021-12-31 #
* 12/31
 unproductive and successful
 called all my family and friend for new year and ate something nice with friends
* 1/1
 ,unproductive and successful
 ,rested all day
* 1/2
 ,unproductive and successful
 ,rested all day, watch some movies with friends
* 1/3
 ,productive and successful
 ,preparing a final presentation for wednesday, a bit stressful because of some members but I could finish my tasks
* 1/4
,unproductive and unsuccessful
,I couldn't finish reading some articles required for a project so I should pick another day to finish them
* 1/5
,productive and successful
,our final presentation was a success everything went smoothly