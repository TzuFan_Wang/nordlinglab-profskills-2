This diary file is written by Christina F94085321 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23 #

* Introduced to GIT Repository (a bit tricky to use, but love the neat and simple layout), wondering why GIT Repository instead of Google Drive.
* Glasl conflict escalation model is interesting.
* Give a presentation and received a lot of feedbacks from Prof (realised the importance of formatting details in making the slides look professional).
* I think exponential might not happen forever, eg. electronic components have physical size limitation, they can't be continually shrunk forever.
* Got new insights about the 5 major pillars of industrial revolution 4.0.
* The Covid-19 pandemic has put us into a setback in reaching SDGs.


# 2021-09-30 #

* Lecture was quite boring
* Why is there so many citation styles?
* How impactful is the presence of fact checking organizations in the midst of information overload? (i.e. how to fact-check a sea of information given the fact that this kind of organizations is not that ubiquitous yet)
* Will there always be more than one news for every event (esp. small events) to crosscheck?
* Talk by Mona Chalabi is insightful: 
	* demonstrated how inaccurate is polling
	* seems like standard deviation (or sth similar to it) should be added to each number so that readers know what to expect on the variability
	* just wondering if data from governmental institution are really more credible (eg. comparing to independent survey institutions)
* Spotting fake news on Facebook: did not spot one on my timeline 
	* news were mainly from verified pages i subscribed
	* suggestions were mostly from "pretty reliable" sources, click-bait content being the worst

# 2021-10-07 #

* Lecture was quite tough, need more time to digest
* Though that theory of banking has been proven long long time ago, but turns out to be just tested in 2014
* Quantity theory of credit:
	* In the midst of pandemic, I guess the loan interest rate for enterprises should be lower
	* How could investment credit work in the absence of consumption credit (i.e., where to sell the goods and services if there is no market)?
	* If asset credit is deemed as unproductive credit creation, will this viscious cycle hit the real estate companies really hard?
* Realized the power of SMEs and decentralization. Is the decentralization of PRC's banking system one of the factor fueling their economic spurt?

# 2021-10-14 #

* Apparently, central bank can help improve people's economy by buying more assets from them (that's why the higher balance is associated with crisis）
* Central bank balance sheets tend to show upward trend. Factors beside crises (eg. inflation, economic growth) should be considered too. Relative to what variable(s) should it be normalized to so that it could better reflects the country's financial condition?
* Feeling a bit unusual that most of the presenting groups (and my own group) used same references, eg. website for central bank balance sheet, PPP
* Just understood what facism is, but still not so clear on the border between facism and nationalism
* Quote of the day: "Bad things are not always ugly, sometimes they are beautiful."

# 2021-10-21 #

* To maintain optimal performance, we need to ensure every building blocks underlying it are working well too
	* physiology --> emotion --> feeling --> thinking --> behaviour --> performance
* Still couldn't understand how others could have better focus and attention after workout while I always end up tired and eventually sleeping
* Have never thought climate change could let so many youngsters feel anxious to the extend that it affects their performance

# 2021-10-28 #

* I was quite shocked by the fact that depression affects >4% of human population and is the leading cause of health issue and disabilities worldwide
* I think giving a depressed friend some room and acquire his/her concent before talking is a good approach
* Seems like there's no clear boundary between just being sad and depressed

# 2021-11-04 #

* TED Talk:
    * Chasing happiness make people unhappy!?
    * Maybe living happily and meaningfully are correlated but not a bi-implication.
    * 4 pillars of meaning: belonging, purpose, transcedence, storytelling
* Insightful talk about the working world:
	* A lot of fresh grads need to have a deeper understanding of the political game (social skills)
	* Although it is commonly believed that new comers are the one who need to fit in, but actually they should also be engaged by the team
	* Just learned that some companies do offer two-sided trial program
	* Toxic working environment will barely contribute to personal growth, so quit asap

# 2021-11-11 #

* Was feeling dizzy that day, didn't catch much of the lecture
* Noisy environment --> production of stress hormones --> increased risk of cardiovascular diseases
* There is a bunch of green building standards and every standards has slighthy different parameters and weighing
* Using natural building materials such as wood may help reduce carbon emission by getting the carbon rich wood into use rather than being decomposed when trees die. Note that this is only true when the forest is periodically replanted.

# 2021-11-25 #

* Regretted that I did not attend the lecture physically, seems like it was a lot fun.
* Intrigued by the fact that TED has been there since 1984.
* What we call social network is not longer a pure social network anymore as more and more behavioral modification features have and are constantly being introduced.
* Cathy O'Neil mentioned the algorithm as weapons of math destruction. I somewhat disagree with her as I think the phenomena happen because the algorithms we have today are not good enough. We should focus on improving it, not trying to eliminate it at all from our daily life.

# 2021-12-02 #

* My first face-to-face class! What a conducive room for workshop.
* Project proposal presentation: some were interesting, especially the noise detector.
* Not so confident about the project (results are expected in 1 month!)
* Felt so clueless listening the history lectures @@
* How many times I looked at my phone? Uhm, I didn't really count it, buy maybe around 1-2 times (?)

# 2021-12-09 #
* I think not all media are biased. For example, the Indonesian media during 2014 and 2019 presidential election campaign periods, some media were inclined to Jokowi (Media Group), some to Prabowo (Visi Media Asia), and there was also the neutral group (Trans Media).
* 'Let the environment guide our development' is the best TED video so far, like the way he explained the concept, esp. the 'a system may have more than one equilibrium point' part.
* Need time to digest WBS, PERT, and Gantt chart.

# 2021-12-16 #
* The 2+ hours debate was quite hectic (but at last it was quite fun)
* Joining the new group
* Gonna catch up with the TED videos soon!

# 2021-12-23 #
* Forget to write down daily diary
* Today (12/26) I am feeling quite productive, preparing for two final presentations next week