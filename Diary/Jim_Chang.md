This diary file is written by Jim Chang in the course Professional skills for engineering the third industrial revolution.

# 2021-9-30 #

* The first lecture was fresh to me honestly.
* I am really inspired and puzzled by the presentation and the diary.
* I am surprised by the Christiane's interview with president Macron.
* I learn a lot about how to improve the stastics and also how to present it more clearly.
* We should always doubt about the statics but not entirely deny the data.
* What Christiane said makes me reflect on myself whether I got trap in the stratosphere created by the social media.
* Newsvoice topic:Humanitarian advocate Mari Malek: Think twice before you donate to big NGO’s
* Billions of dollars every year are given to huge NGOs working in Africa yet famine, wars and poverty continue that create suffering and inequality.
* More people die from starvation rather than Covid-19, At the same time, Africa is extremely efficient in food production, which is a paradox.
* We should not donate our money to the huge NGOs even though they may seem more credible.
* We can try to believe and donate our money to some little humanitarian organizations since what they do may be more transparent and pratical.

# 2021-10-7 #

* It's unfortunately that my team doesn't get the chance to representation.
* The new team work has started, I hope my teammate can be efficient and diligent.
* I'm surpised to this week's lecture is about economics.
* I learn a lot about loan and also a new terminology so-called quantity easing.
* After the class and the searching afterclass, I have more thoughts about the phenomenon and how it works.
* Still, I believe what give the currency prices is no doubt---human desire.

# 2021-10-14 #

* It's unfortunate that I didn't get the chance of presentation.
* Using Youtube to upload the video is so surprising that I'm still be at a loss honestly.
* Hoping my first time making the video will be all right.
* Yuval Noah Harari remind us that the advanced of technology may strength some dictatorship government's power.
* It's so amazing to see a white supremacist to get out and turn to a Egalitarianismist.
* Christian Picciolini teach me an important lesson that I should open my heart to embarce the one who I used to not understand.
* An interesting task launch by Christian Picciolini, find something or someone who I think is undesrving of my compassion and give it to them.
* If plenty of people available to do the task occasionally, our world may be different and the tragedy may not happen that frequently.

# 2021-10-21 #
* Not until professer open my video that I realize my video has some trouble.
* A little depressed that I didn't do my job well.
* It's not surprise for me that exercise really bring benefit to our health, since I have read some paper before.
* I fully agree with Matthias Müllenbeck's view point.
* It's without a doubt that we better prevent unhealthy behavior and keep ourselves healthy rather than eveything is too late.

# 2021-10-28 #
* It's a fresh experience for me to make a slide with some unknown people in such emergency time.
* Thanks for my supergroup's mate, for they all help active to complete the presentation.
* It's quite upset that someone didn't join the cooperation, lest we could finish our task more perfectly.
* I have never meet one that have depression, but I have knew a steamer who have severe depressed trouble, and besides his disease happen, he really act like an original person.
* We should not treat the depression like a stranger, for they still live originally, only when they're disease happened, they may act a little different to the others.
* For most mental disease, the most important thing is company; Though everyone know it, the most difficult point is when and how.
* If I have depression I not sure whether I have the courage to admit it, it so appreciate that Nikki Webber Allen can accept it and face it in the tough and infriendly environment.
* The people who want to suicide may just want to end the pain they've suffer, or just the pressure is too heavy to them to handle. If there's someone there to hear they trouble or the story happened on them, then we may have the chance to stop the tragedy from happening.

# 2021-11-4 #
* It really shocked me that so many classmate are under a large pressure and feel axious frequently.
* I absolutely support professor to survey the mental condition of NCKU, for I really curious about wheather the other students also suffer in such negative condition as well,
* Hope everyone can get there own method to get away from their trouble, anxiety or even depression.
* Petter Johansson's experiment inspired me a lot.
* It's very funny that people were tned to accept what they get, which may not exactly they choose.
* However, in fact, we human often persuade ourselve to have good impress on the thing get or the person in group or as a partner, which is very interesting.

# 2021-11-11 #
* When it comes to the midterm, gourp members are often out of time to have the discussion or even complete their own work.
* After the presentation and the slide making, I have deeper comprehansive of the noise pollution.
* The noise pollution really cause a more negative impact on our mental and concentration or even our phiscal health, even now I'm troubled by the noise made by the outside concert.
* Each group has collect many imformations about the topic the are assign to do.
* I thought it's too tight too listen all groups' presentation, we can't have much time to think twice, remind of the slide, and absorb it.
* It's a liitle sad that the lesson is out of time to learn some new knowledge about law.

# 2021-11-25 #
* no until today that I remember I've forget to write the diary. :(
* I've bee through a busy week.
* I feel really preasure these days, but I've find my to release my stess.
* Phiscal class really surprised me, and the high attendence is out of my surprise as well.
* Hope the video we will watch in the future can be played with lyrics.

# 2021-12-2 #
* Each group's ideas are too ideal to be implemented as we are just poor student.
* The idea and the actions we proposed two weeks ago are actually pratical than other groups.
* It's surprise to me that professor can show the annoucement of the Tainan environmental authorities.
* Not until today that I knew that there's a new dorm being building near the teacher's dorm, and it noise the professors as well.
* Our dorm has the construction noise as well, it's really disturbtion when I try to focus on my task or still sleepy.
* our group discuss another pratical actions, which is change the textbook into PDF format.
* Hope the action can no only improve we students' health, but also change our pocket.

# 2021-12-9 #
* Today I learn a plenty of country's news website.
* I know what exactly professor ask us to do, and realize what's the effect of analysis the topic and their relationship.
* We discuss a news and address our perspective and illustrate which group of people will be benefit from the news.
* We missunderstand the question b and c, so our group give the wrong opinnion, but fortunately, our group have ask professor what the problem is in our opinion, so we can recorrect our answer. 
* Maybe the task next week is quite difficult to complete, but I believe my supergroup can conquer it.

# 2021-12-16 #
* This is my first time to presentation phiscally, I feel really nervous.
* Professor must be very care about the task, for he upgrade his require in today's presentation.
* I feel really embarrassed at the final of my presentation, for my slides keep ending automatically for unknown reason.
* I think each group have plenties of things to improve, for our result or presentation is not meet professor's requirement.

# 2021-12-23 #
* Today, professor use the timer to make sure every groups finish their presentation in time, the action makes us feel a little nervous.
* Every group's presentation have illustrate their idea clearly by answering those answers.
* Maybe I didn't did the slide well, our group's presentation is out of time.
* The debate in class is interesting and inspiring, for we can think about the issues more and realize more deeply about the character we act.
* Too upset that the capital choose to earn money rather than social equality.
* Today is a unsuccessful and unproductive day.
* For I got a cold two days ago, and I fell really weak these days, my mind is really slow, and I can't think many things now.
* What a awful day, hope tomorrow will be better.

# 2021-12-24 #
* Today is a successful but unproductive day.
* I'm still weak now, and can't do things clearly.
* But I have plenty of time to finish the work I plan to do; thus, I think today I'm successful.
* However, I still didn't finish the task I can complete when I'm healthy which is quite unfortunate.
* I should go to bed early to make me recover from the disease more quickly.

# 2021-12-25 #
* Merry Christmas!!!
* Though, I thought is an unsuccessful but productive day.
* For I still not recover from the disease, I thought my life is too awful.
* Besides, everyone has the activity in Christmas, but I can't any place, for I'm still weak.
* Nevertheless, I did many task than yesterday, for I have a better body condition than yesterday and I finish most of the work I planned.
* I can remain the situation today, but I think join some activity may be important to my life as well.

# 2021-12-26 #
* Today is a successful but unproductive day.
* Today is a sucessful day for I think I my body is almost recover from the sickness, and now I can try something sweat or freeze.
* That's so fantastic.
* The reason why today is unproductive because I wake up too late, and I have other plans; thus, I can't have much time too finish other tasks.
* A good daily routine must be remain and be a custom.

# 2021-12-27 #
* Today is a successful but unproductive day.
* I finish my English course's final presentation, though the presentation may have something inperfect, there still one thing out of my burden.
* I can finish more things today, however, after the presentation, all I want to do is relax.
* Though taking a rest is important, I shouldn't be too dispirited.

# 2021-12-28 #
* Today is a successful but still unproductive day.
* I meet a lot of new friends, and finish a task which have bother me for a long time.
* However, as yesterday, after finish the task, what I want is having a full relax.
* Therefore, I did nothing but lying on my bed, watching the stream, and the day off.
* I make the same mistake as yesterday, I should find a new method to fix it.
* Maybe I can make a more clear plan for a further future.

# 2021-12-29 #
* Today is a successful but still unproductive day.
* For I finally go to bar, having a well experience.
* Besides, I do the experiment effciently, which makes me full of accomplishment.
* Thus, I thought I have a succesful day.
* However, I try to figure out a task that have troubled me many days, I spent most of my day on it, but still didn't complete it, and didn't do anything else as well.
* How a unproductive day.
* Rule 1: Maitain a good mood.
* Rule 2: Having a well daily routine.
* Rule 3: A appropriate rest.
* Rule 4: Having a more complete plan.
* Rule 5: A health body, which is the most important one for all of the five.

# 2021-12-30 #
* Most People in class have the similar conclusion of their five rules.
* Such as wake up early, read some books, or mantain health, etc.
* Our groups project face a huge problem, hope everything will be all right.
* Others groups seems have a clear object and do the things well, hope every group can obtain their goal.
* Hope the action can no only improve we students' health, but also change our pocket. 
