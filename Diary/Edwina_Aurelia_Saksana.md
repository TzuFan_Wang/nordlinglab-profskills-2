This diary file is written by Edwina Aurelia Saksana E14075142 in the course Professional skills for engineering the third industrial revolution.

# 2021-09-23

* First lecture and presentation day.
* I think exponential growth mostly applies to futuristic innovations.
* Exponential growth would stop when newer and better versions of the innovations appear.
* Professor teaches us that doing things according to templates is very important.

# 2021-09-30

* Professor uses Google Meet because it has subtitles feature and I think it's really thoughtful of Professor.
* More presentations, with some mistakes but we've gotten better from previous presentation.
* I learned how to verify news whether it's fake or real.
* It is important to verify news in order to prevent the spreading of rumours.

# 2021-10-7

* I like to learn more about finance, even if I'm majoring in Mechanical Engineering, because I think understanding how finance works is a very important aspect in life, that everyone should understand.
* Though Taiwanese finance is not so important to a foreigner like me, but I think knowing the basic knowledge of the world's finance is also important.
* A country's economy can inflate and deflate depending on debts and loans.
* During this pandemic, many people lose their jobs and have to depend on loans before finding new jobs, making so many countries' economy unstable.

# 2021-10-14

* We are currently living in fictional reality, some fictions are being made to reality, people live based on myths and superstitions.
* Fictional reality is an imagined version of our life that we hope or think is real.
* Even if fictions might not be true, there will always be lessons learned from why they were ever made in first place.
* Basically our lives and future lie in the hands of our government, or politicians in our country.
* From my home country, Indonesia, famous politicians have promised a lot of good things since decades ago, which I think would not be a problem if corruption in my country has ended.
* I think Taiwan's president also promises a very good future for Taiwanese, to be an independent country. Also, Taiwan is a rich country, so I think these promises are easier to achieve and to be done.

# 2021-10-21

* Professor talked about the physical experiment for reading heart rates and it's interesting.
* There are many ways for us to be healthy, not just sleeping well and eating well.
* By controlling our emotions, we can change our feelings in order to change our thinking, giving us more brilliant behaviour outcome.

# 2021-10-28

* Depression is not a joking matter, it is difficult to understand what everyone is going through.
* We have to pay attention to our mental healthas well as the mental health of the people around us, such as family and friends.
* Identifying depression in people is not as easy as identifying genders.
* We have to know what to do to help them with depression, even if it's not much, the little things and the good thoughts are what helps.

# 2021-11-4

* Professor said that social media is not recommended for people with depression because it acts like alcohol, the more likes we get the happier we are.
* I used to have a breakdown and I decided to take a break from social media for a few months, it actually helped me to stop being insecure.
* I think it is best to stay away from anything toxic in life during a breakdown, to prevent falling into depression, at least until you get yourself together again.

# 2021-11-11

* We watched all of the big teams' presentations about the course project.
* All of the presentations are quite long, but they're mostly still introduction, so they were all still kind of simple.
* One of the presentations talked about sleep being very important in our studies, as enough sleep helps us focus and concentrate better in class, which could help us improve our grades.
* Sleep is crucial in improving our memory.

# 2021-11-18
* No class today.

# 2021-11-25
* Internet should be publicly available, to avoid inequity, but to maintain enterpreneurship for when internet is free, ads are invented, even if ads are annoying.
* Social media is toxic, because we stress a lot when our instagram likes decrease.
* Teenagers nowadays rely on friends and social media than families.
* Social media and cell phones are actually not bad, it's the imbalance / addiction that makes them toxic.

# 2021-12-02

* We listened to supergroup presentations, and everyone did great.
* My team did not quite get the assignment, so our presentation was not feasible, but we discussed it in class and got new better ideas.
* I hope whatever our classmates planned for the project could be done immediately and make our environment a better place.

# 2021-12-09

* I think it was kind of difficult to compare countries because we don't understand their cultures and languages fully.
* Defining perspectives is also difficult because it depends on subjective views instead of objective.
* Most news reports are based on the authors' perspectives and are subjective, usually siding from one point of view only.

# 2021-12-16

* It's actually surprising that having one fewer child reduces 60 tons of CO2 per year, higher than ang other pollitive causes.
* Live car free, meaning that having one fewer car, only reduces 2.5 tons of CO2 per year, 24 times less than having one fewer child.
* It is really pitiful that many countries do not educate people well about these differences. It was seen in a research on a country's (if I'm not mistaken it's Canada) school books, mentioning almost zero things about having one fewer child.
* I think this needs to be a global concern too, and the world's governments could make effort in giving out programs for people to have fewer children.

# 2021-12-23

# 2021-12-24

# 2021-12-25

# 2021-12-26
